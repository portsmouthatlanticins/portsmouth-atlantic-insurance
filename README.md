Some of our clients tell us that they feel very comfortable working with our family owned agency because each agent has a vested interest in what we do. We'd like to think that it is more than just our particular relationship with each other, but we have to admit that our unique bond gives a certain meaning to our work and clients. A meaning that is illustrated through the companies we represent. 

We don't represent every insurance company, but have searched high and low for companies that will service you when you are in need or even when you're not. Our search was focused on customer and claims service. Regardless of the premium you pay and the service you get, there is no better feeling than to have a company by your side during a claim. After all, this is what you pay insurance for. Great service and price. Almost all of the insurance carriers we represent have an excellent or better rating with A.M. Best. Some carriers have been nationally recognized by J.D. Power & Associates for their excellent service. 

As a full service agency, we can handle all individual insurance needs including Personal Automobile & Homeowners, Condo, Recreational Vehicles, Personal Umbrella, Mobile Home, and Rental Property. 

We also provide Business Owners packages, Professional Liability, Workers Compensation, Umbrella, Commercial Auto and Bonds. 

We look forward to working with you!